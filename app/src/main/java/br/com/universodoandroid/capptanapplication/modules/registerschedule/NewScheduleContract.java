package br.com.universodoandroid.capptanapplication.modules.registerschedule;

import br.com.universodoandroid.capptanapplication.modules.BaseView;

public interface NewScheduleContract {
    interface View extends BaseView<Presenter> {
        void onFinishPost();
    }

    interface Presenter {
        void registerSchedule(String title, String description, String details, String author);
    }
}
