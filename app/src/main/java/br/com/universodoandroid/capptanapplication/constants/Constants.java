package br.com.universodoandroid.capptanapplication.constants;

public class Constants {

    public static final String KEY_USER_NAME = "name";
    public static final String KEY_USERS_DATABASE_REFERENCE = "users";
    public static final String KEY_SCHEDULES_DATABASE_REFERENCE = "schedules";

    public static final String KEY_SCHEDULES_TITLE = "title";
    public static final String KEY_SCHEDULES_DESCRIPTION = "description";
    public static final String KEY_SCHEDULES_DETAILS = "details";
    public static final String KEY_SCHEDULES_AUTHOR = "author";
    public static final String KEY_SCHEDULES_STATUS = "status";
    public static final String STATUS_FALSE = "false";
    public static final String STATUS_TRUE = "true";

    public static final String KEY_LOGGED_IN = "logged_in";
    public static final String KEY_LOGGED_IN_DATABASE_REFERENCE = "user_reference";

}
