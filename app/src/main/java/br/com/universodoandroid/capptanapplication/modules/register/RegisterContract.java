package br.com.universodoandroid.capptanapplication.modules.register;

import br.com.universodoandroid.capptanapplication.modules.BaseView;

public interface RegisterContract {
    interface View extends BaseView<Presenter> {
        void onRegisterSuccess();
        void onRegisterFailure(String message);
    }

    interface Presenter {
        void signUpWithEmailAndPassword(String name, String email, String password);
    }
}
