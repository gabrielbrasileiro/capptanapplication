package br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules.closed;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.universodoandroid.capptanapplication.R;
import br.com.universodoandroid.capptanapplication.domain.Schedule;
import br.com.universodoandroid.capptanapplication.modules.menu.MenuActivity;
import br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules.ScheduleFragment;
import br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules.adapter.ScheduleAdapter;

import static br.com.universodoandroid.capptanapplication.constants.Constants.STATUS_TRUE;
import static com.annimon.stream.Optional.ofNullable;

public class ScheduleFragmentClosed extends ScheduleFragment {

    private ScheduleAdapter mSchedulerAdapter;

    public static Fragment newInstance() {
        return new ScheduleFragmentClosed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ofNullable(((MenuActivity) getActivity())).ifPresent(activity ->
                activity.updateToolbarTitle(getString(R.string.fragment_schedules_closed_title)));

        mSchedulerAdapter = new ScheduleAdapter(getPresenter());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected ScheduleAdapter getSchedulersAdapter() {
        return mSchedulerAdapter;
    }

    @Override
    protected String getStatusView() {
        return STATUS_TRUE;
    }

    @Override
    public void loadSchedulers(List<Schedule> schedules) {
        mSchedulerAdapter.clearData();
        mSchedulerAdapter.addAll(this, STATUS_TRUE, schedules);
    }

}
