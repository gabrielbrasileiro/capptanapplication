package br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.universodoandroid.capptanapplication.R;
import br.com.universodoandroid.capptanapplication.databinding.ScheduleItemBinding;
import br.com.universodoandroid.capptanapplication.domain.Schedule;
import br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules.ScheduleContract;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static br.com.universodoandroid.capptanapplication.constants.Constants.STATUS_FALSE;
import static br.com.universodoandroid.capptanapplication.constants.Constants.STATUS_TRUE;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ItemViewHolder> {

    private ScheduleContract.Presenter mPresenter;
    private List<Schedule> mSchedules;

    public ScheduleAdapter(ScheduleContract.Presenter presenter) {
        mSchedules = new ArrayList<>(0);
        mPresenter = presenter;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.schedule_item,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder viewHolder, int position) {
        Schedule schedule = mSchedules.get(position);
        Context context = viewHolder.mScheduleItemBinding.getRoot().getContext();

        Button finalizeScheduleButton = viewHolder.mScheduleItemBinding.finalizeScheduleButton;
        TextView authorTextView = viewHolder.mScheduleItemBinding.authorTextView;

        if (finalizeScheduleButton.getVisibility() == VISIBLE) {
            setExpansionStatus(finalizeScheduleButton, authorTextView);
        }

        viewHolder.mScheduleItemBinding.scheduleItemCardView.setOnClickListener(v ->
                setExpansionStatus(finalizeScheduleButton, authorTextView));

        if (schedule.getStatusBoolean()) {
            finalizeScheduleButton.setText(context.getString(R.string.schedule_adapter_reopen));
            finalizeScheduleButton.setOnClickListener(
                    viewButton -> mPresenter.updateScheduleStatus(schedule.getId(), STATUS_FALSE));
        } else {
            finalizeScheduleButton.setText(context.getString(R.string.schedule_adapter_finalize));
            finalizeScheduleButton.setOnClickListener(
                    viewButton -> mPresenter.updateScheduleStatus(schedule.getId(), STATUS_TRUE));
        }

        viewHolder.updateView(schedule);
    }

    private void setExpansionStatus(Button button, TextView textView) {
        boolean buttonIsVisible = button.getVisibility() == VISIBLE;
        boolean textViewIsVisible = textView.getVisibility() == VISIBLE;

        if (buttonIsVisible && textViewIsVisible) {
            button.setVisibility(GONE);
            textView.setVisibility(GONE);
        } else {
            button.animate();
            button.setVisibility(VISIBLE);
            textView.setVisibility(VISIBLE);
        }
    }

    private void add(Schedule schedule) {
        mSchedules.add(schedule);
        notifyItemInserted(mSchedules.size());
    }

    public void addAll(ScheduleContract.View view, String status, List<Schedule> schedules) {
        for (Schedule schedule : schedules) {
            if (status.equals(schedule.getStatus())) {
                add(schedule);
            }
        }

        if (mSchedules.isEmpty()) {
            view.setMessageEmptyVisibility(VISIBLE);
        } else {
            view.setMessageEmptyVisibility(GONE);
        }
    }

    public void clearData() {
        mSchedules = new ArrayList<>();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mSchedules.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private ScheduleItemBinding mScheduleItemBinding;

        ItemViewHolder(ScheduleItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.mScheduleItemBinding = itemBinding;
        }

        void updateView(Schedule schedule) {
            this.mScheduleItemBinding.setSchedule(schedule);
            this.mScheduleItemBinding.executePendingBindings();
        }
    }

}
