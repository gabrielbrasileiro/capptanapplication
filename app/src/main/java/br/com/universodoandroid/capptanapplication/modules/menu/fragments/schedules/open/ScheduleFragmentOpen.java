package br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules.open;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.util.List;

import br.com.universodoandroid.capptanapplication.R;
import br.com.universodoandroid.capptanapplication.domain.Schedule;
import br.com.universodoandroid.capptanapplication.modules.menu.MenuActivity;
import br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules.ScheduleFragment;
import br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules.adapter.ScheduleAdapter;

import static br.com.universodoandroid.capptanapplication.constants.Constants.STATUS_FALSE;
import static com.annimon.stream.Optional.ofNullable;

public class ScheduleFragmentOpen extends ScheduleFragment {

    private ScheduleAdapter mSchedulerAdapter;

    public static Fragment newInstance() {
        return new ScheduleFragmentOpen();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ofNullable(((MenuActivity) getActivity())).ifPresent(activity ->
                activity.updateToolbarTitle(getString(R.string.fragment_schedules_open_title)));

        mSchedulerAdapter = new ScheduleAdapter(getPresenter());
    }

    @Override
    protected ScheduleAdapter getSchedulersAdapter() {
        return mSchedulerAdapter;
    }

    @Override
    protected String getStatusView() {
        return STATUS_FALSE;
    }

    @Override
    public void loadSchedulers(List<Schedule> schedules) {
        mSchedulerAdapter.clearData();
        mSchedulerAdapter.addAll(this, STATUS_FALSE, schedules);
    }

}
