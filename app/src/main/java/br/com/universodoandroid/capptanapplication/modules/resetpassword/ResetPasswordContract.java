package br.com.universodoandroid.capptanapplication.modules.resetpassword;

import br.com.universodoandroid.capptanapplication.modules.BaseView;

public interface ResetPasswordContract {
    interface View extends BaseView<Presenter> {
        void onRegisterSuccess();
        void onRegisterFailure(String message);
    }

    interface Presenter {
        void resetPassword(String email);
    }
}
