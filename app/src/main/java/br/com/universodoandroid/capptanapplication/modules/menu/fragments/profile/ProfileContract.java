package br.com.universodoandroid.capptanapplication.modules.menu.fragments.profile;

import br.com.universodoandroid.capptanapplication.domain.User;
import br.com.universodoandroid.capptanapplication.modules.BaseView;

public interface ProfileContract {
    interface View extends BaseView<Presenter> {
        void setUserInformation(User user);
    }

    interface Presenter {
        void getUserInformation();
        void logout();
    }
}
