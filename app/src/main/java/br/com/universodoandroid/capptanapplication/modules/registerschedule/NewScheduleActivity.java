package br.com.universodoandroid.capptanapplication.modules.registerschedule;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import br.com.universodoandroid.capptanapplication.R;
import br.com.universodoandroid.capptanapplication.databinding.ActivityNewScheduleBinding;
import br.com.universodoandroid.capptanapplication.services.FirebaseDatabaseProvider;
import br.com.universodoandroid.capptanapplication.utils.MultiTextWatcher;

public class NewScheduleActivity extends AppCompatActivity implements NewScheduleContract.View {

    private ActivityNewScheduleBinding mActivityNewScheduleBinding;
    private ProgressDialog mProgressDialog;
    private NewScheduleContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new NewSchedulePresenter(this, new FirebaseDatabaseProvider(this));

        mActivityNewScheduleBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_schedule);
        mActivityNewScheduleBinding.setHandler(this);

        mActivityNewScheduleBinding.backButton.setOnClickListener(v -> finish());

        configureRegisterClick();

        checkTextViews(mActivityNewScheduleBinding.titleEditText,
                mActivityNewScheduleBinding.descriptionEditText,
                mActivityNewScheduleBinding.detailsEditText,
                mActivityNewScheduleBinding.authorEditText);
    }

    private void configureRegisterClick() {
        mActivityNewScheduleBinding.registerButton.setOnClickListener(v -> {
            String title = mActivityNewScheduleBinding.titleEditText.getText().toString().trim();
            String description = mActivityNewScheduleBinding.descriptionEditText.getText().toString().trim();
            String details = mActivityNewScheduleBinding.detailsEditText.getText().toString().trim();
            String author = mActivityNewScheduleBinding.authorEditText.getText().toString().trim();

            mPresenter.registerSchedule(title, description, details, author);
        });

        mActivityNewScheduleBinding.registerButton.setEnabled(false);
        mActivityNewScheduleBinding.registerButton.setBackgroundColor(ContextCompat.getColor(this, R.color.grey));
    }

    private void checkTextViews(EditText... editTexts) {
        MultiTextWatcher multiTextWatcher = new MultiTextWatcher(
                editTexts, mActivityNewScheduleBinding.registerButton);

        for (EditText editText : editTexts) {
            editText.addTextChangedListener(multiTextWatcher);
        }
    }

    @Override
    public void setPresenter(NewScheduleContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showProgressBar() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.dialog_message));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    public void dismissProgressBar() {
        mProgressDialog.dismiss();
    }

    @Override
    public void onFinishPost() {
        finish();
    }

}
