package br.com.universodoandroid.capptanapplication.modules.resetpassword;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import br.com.universodoandroid.capptanapplication.R;
import br.com.universodoandroid.capptanapplication.databinding.ActivityResetPasswordBinding;
import br.com.universodoandroid.capptanapplication.services.FirebaseAuthProvider;

public class ResetPasswordActivity extends AppCompatActivity implements ResetPasswordContract.View {

    private ActivityResetPasswordBinding mActivityResetPasswordBinding;

    private ResetPasswordContract.Presenter mPresenter;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new ResetPasswordPresenter(this, new FirebaseAuthProvider(this));

        mActivityResetPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password);
        mActivityResetPasswordBinding.setHandler(this);

        mActivityResetPasswordBinding.resetEmailButton.setOnClickListener(v -> {
            String email = mActivityResetPasswordBinding.emailTextView.getText().toString();

            if (!email.isEmpty()) {
                mPresenter.resetPassword(email);
            } else {
                Toast.makeText(this, getString(R.string.activity_reset_verify_email_field), Toast.LENGTH_SHORT).show();
            }
        });

        mActivityResetPasswordBinding.returnButton.setOnClickListener(v -> finish());
    }

    @Override
    public void onRegisterSuccess() {
        Toast.makeText(this, getString(R.string.activity_reset_verify_your_email), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onRegisterFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(ResetPasswordContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showProgressBar() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.dialog_message));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    public void dismissProgressBar() {
        mProgressDialog.dismiss();
    }

}
