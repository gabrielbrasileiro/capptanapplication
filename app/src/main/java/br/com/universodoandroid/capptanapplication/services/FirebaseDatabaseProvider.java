package br.com.universodoandroid.capptanapplication.services;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import br.com.universodoandroid.capptanapplication.domain.Schedule;
import br.com.universodoandroid.capptanapplication.domain.User;
import br.com.universodoandroid.capptanapplication.modules.menu.MenuActivity;
import br.com.universodoandroid.capptanapplication.utils.RequestCallback;

import static br.com.universodoandroid.capptanapplication.constants.Constants.KEY_SCHEDULES_AUTHOR;
import static br.com.universodoandroid.capptanapplication.constants.Constants.KEY_SCHEDULES_DATABASE_REFERENCE;
import static br.com.universodoandroid.capptanapplication.constants.Constants.KEY_SCHEDULES_DESCRIPTION;
import static br.com.universodoandroid.capptanapplication.constants.Constants.KEY_SCHEDULES_DETAILS;
import static br.com.universodoandroid.capptanapplication.constants.Constants.KEY_SCHEDULES_STATUS;
import static br.com.universodoandroid.capptanapplication.constants.Constants.KEY_SCHEDULES_TITLE;
import static br.com.universodoandroid.capptanapplication.constants.Constants.KEY_USERS_DATABASE_REFERENCE;
import static br.com.universodoandroid.capptanapplication.constants.Constants.KEY_USER_NAME;
import static br.com.universodoandroid.capptanapplication.constants.Constants.STATUS_FALSE;

public class FirebaseDatabaseProvider {

    private Activity mActivity;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseUsers;
    private DatabaseReference mDatabaseSchedules;

    public FirebaseDatabaseProvider(Activity activity) {
        mActivity = activity;
        mAuth = FirebaseAuth.getInstance();

        mDatabaseUsers = FirebaseDatabasePersistence.getDatabase().getReference().child(KEY_USERS_DATABASE_REFERENCE);

        if (mAuth.getUid() != null) {
            mDatabaseSchedules = FirebaseDatabasePersistence.getDatabase().getReference()
                    .child(KEY_USERS_DATABASE_REFERENCE)
                    .child(mAuth.getUid())
                    .child(KEY_SCHEDULES_DATABASE_REFERENCE);

            mDatabaseSchedules.keepSynced(true);
        }

        mDatabaseUsers.keepSynced(true);
    }

    public void checkUserSettingsExists(RequestCallback.Database<Intent> callback) {
        initializeEventListener(mDatabaseUsers, new ServicesCallback() {
            @Override
            public void onSuccessResult(DataSnapshot dataSnapshot, FirebaseUser firebaseUser) {
                Intent intent;
                Class classReference;

                intent = new Intent(mActivity, MenuActivity.class);
                classReference = MenuActivity.class;

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                callback.onSuccess(intent, classReference);
            }

            @Override
            public void onFailureResult(String errorMessage) {
                callback.onError(errorMessage);
            }
        });
    }

    public void getUserInformation(RequestCallback<User> callback) {
        DatabaseReference databaseUserInformation = mDatabaseUsers.child(mAuth.getCurrentUser().getUid());

        initializeEventListener(databaseUserInformation, new ServicesCallback() {
            @Override
            public void onSuccessResult(DataSnapshot dataSnapshot, FirebaseUser firebaseUser) {
                String name = (String) dataSnapshot.child(KEY_USER_NAME).getValue();

                callback.onSuccess(new User(name, firebaseUser.getEmail()));
            }

            @Override
            public void onFailureResult(String errorMessage) {
                callback.onError(errorMessage);
            }
        });
    }

    public void registerUserInformation(String uid, String name) {
        mDatabaseUsers.child(uid).child(KEY_USER_NAME).setValue(name);
    }

    public void registerScheduleInformation(String title, String description, String details, String author) {
        DatabaseReference databaseReference = mDatabaseSchedules.push();

        databaseReference.child(KEY_SCHEDULES_TITLE).setValue(title);
        databaseReference.child(KEY_SCHEDULES_DESCRIPTION).setValue(description);
        databaseReference.child(KEY_SCHEDULES_DETAILS).setValue(details);
        databaseReference.child(KEY_SCHEDULES_AUTHOR).setValue(author);
        databaseReference.child(KEY_SCHEDULES_STATUS).setValue(STATUS_FALSE);
    }

    public void updateScheduleStatus(String id, String status) {
        mDatabaseSchedules.child(id).child(KEY_SCHEDULES_STATUS).setValue(status);
    }

    public void provideSchedules(RequestCallback<List<Schedule>> callback) {
        mDatabaseSchedules.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Schedule> translations = new ArrayList<>();

                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    translations.add(new Schedule(childDataSnapshot.getKey(),
                            (String) childDataSnapshot.child(KEY_SCHEDULES_TITLE).getValue(),
                            (String) childDataSnapshot.child(KEY_SCHEDULES_DESCRIPTION).getValue(),
                            (String) childDataSnapshot.child(KEY_SCHEDULES_DETAILS).getValue(),
                            (String) childDataSnapshot.child(KEY_SCHEDULES_AUTHOR).getValue(),
                            (String) childDataSnapshot.child(KEY_SCHEDULES_STATUS).getValue()
                    ));
                }

                callback.onSuccess(translations);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.onError(databaseError.getMessage());
            }
        });
    }

    private void initializeEventListener(DatabaseReference databaseReference, ServicesCallback callback) {
        if (mAuth.getCurrentUser() != null) {
            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    callback.onSuccessResult(dataSnapshot, mAuth.getCurrentUser());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    callback.onFailureResult(databaseError.getMessage());
                }
            });
        } else {
            callback.onFailureResult("Don't have any user");
        }
    }

}
