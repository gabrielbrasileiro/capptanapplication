package br.com.universodoandroid.capptanapplication.modules;

public interface BaseView<T> {
    void setPresenter(T presenter);
    void showProgressBar();
    void dismissProgressBar();
}
