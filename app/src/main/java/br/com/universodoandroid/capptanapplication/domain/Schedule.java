package br.com.universodoandroid.capptanapplication.domain;

import static br.com.universodoandroid.capptanapplication.constants.Constants.STATUS_TRUE;

public class Schedule {

    private String uid;
    private String title;
    private String description;
    private String details;
    private String author;
    private String status;

    public Schedule(String uid, String title, String description, String details, String author, String status) {
        this.uid = uid;
        this.title = title;
        this.description = description;
        this.details = details;
        this.author = author;
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDetails() {
        return details;
    }

    public String getAuthor() {
        return author;
    }

    public String getStatus() {
        return status;
    }

    public boolean getStatusBoolean() {
        return status.equals(STATUS_TRUE);
    }

    public String getId() {
        return uid;
    }


}
