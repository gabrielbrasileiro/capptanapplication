package br.com.universodoandroid.capptanapplication.modules.registerschedule;

import android.support.annotation.NonNull;

import br.com.universodoandroid.capptanapplication.services.FirebaseDatabaseProvider;

public class NewSchedulePresenter implements NewScheduleContract.Presenter {

    private FirebaseDatabaseProvider mFirebaseDatabaseProvider;
    private NewScheduleContract.View mView;

    public NewSchedulePresenter(@NonNull NewScheduleContract.View view,
                                @NonNull FirebaseDatabaseProvider firebaseDatabaseProvider) {
        mView = view;
        mFirebaseDatabaseProvider = firebaseDatabaseProvider;

        mView.setPresenter(this);
    }

    @Override
    public void registerSchedule(String title, String description, String details, String author) {
        mFirebaseDatabaseProvider.registerScheduleInformation(title, description, details, author);
        mView.onFinishPost();
    }

}
