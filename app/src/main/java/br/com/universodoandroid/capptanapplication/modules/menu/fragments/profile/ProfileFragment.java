package br.com.universodoandroid.capptanapplication.modules.menu.fragments.profile;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.universodoandroid.capptanapplication.R;
import br.com.universodoandroid.capptanapplication.database.Session;
import br.com.universodoandroid.capptanapplication.databinding.FragmentProfileBinding;
import br.com.universodoandroid.capptanapplication.domain.User;
import br.com.universodoandroid.capptanapplication.modules.menu.MenuActivity;
import br.com.universodoandroid.capptanapplication.services.FirebaseAuthProvider;
import br.com.universodoandroid.capptanapplication.services.FirebaseDatabaseProvider;

import static com.annimon.stream.Optional.ofNullable;

public class ProfileFragment extends Fragment implements ProfileContract.View {

    private FragmentProfileBinding mFragmentProfileBinding;
    private ProfileContract.Presenter mPresenter;

    public static Fragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ofNullable(((MenuActivity) getActivity())).ifPresent(activity ->
                activity.updateToolbarTitle(getString(R.string.fragment_profile_title)));

        new ProfilePresenter(this,
                new FirebaseAuthProvider(getActivity()),
                new FirebaseDatabaseProvider(getActivity()),
                new Session(getActivity()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        mFragmentProfileBinding.setHandler(this);
        setInformationVisibility(View.GONE);

        mPresenter.getUserInformation();

        mFragmentProfileBinding.logoutButton.setOnClickListener(view -> mPresenter.logout());

        return mFragmentProfileBinding.getRoot();
    }

    @Override
    public void setPresenter(ProfileContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showProgressBar() {
        mFragmentProfileBinding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgressBar() {
        mFragmentProfileBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setUserInformation(User user) {
        mFragmentProfileBinding.userNameTextView.setText(user.getName());
        mFragmentProfileBinding.userEmailTextView.setText(user.getEmail());
        setInformationVisibility(View.VISIBLE);
    }

    private void setInformationVisibility(int visibility) {
        mFragmentProfileBinding.userNameTextView.setVisibility(visibility);
        mFragmentProfileBinding.userEmailTextView.setVisibility(visibility);
    }

}
