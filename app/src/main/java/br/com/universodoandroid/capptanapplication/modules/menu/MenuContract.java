package br.com.universodoandroid.capptanapplication.modules.menu;

import android.content.Intent;

import br.com.universodoandroid.capptanapplication.modules.BaseView;

public interface MenuContract {
    interface View extends BaseView<Presenter> {
        void setCorrectIntentByUserState(Intent intent, Class classReference);
        void updateToolbarTitle(String name);
    }

    interface Presenter {
        void checkUserSettings();
    }
}
