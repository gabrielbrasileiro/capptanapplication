package br.com.universodoandroid.capptanapplication.modules.register;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import br.com.universodoandroid.capptanapplication.R;
import br.com.universodoandroid.capptanapplication.databinding.ActivityRegisterBinding;
import br.com.universodoandroid.capptanapplication.modules.register.RegisterContract.Presenter;
import br.com.universodoandroid.capptanapplication.services.FirebaseAuthProvider;
import br.com.universodoandroid.capptanapplication.services.FirebaseDatabaseProvider;

public class RegisterActivity extends AppCompatActivity implements RegisterContract.View {

    private ActivityRegisterBinding mActivityRegisterBinding;
    private Presenter mPresenter;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new RegisterPresenter(this, new FirebaseAuthProvider(this), new FirebaseDatabaseProvider(this));

        mActivityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        mActivityRegisterBinding.setHandler(this);

        mActivityRegisterBinding.registerButton.setOnClickListener(view -> {
            String userName = mActivityRegisterBinding.userNameEditText.getText().toString();
            String email = mActivityRegisterBinding.loginEditText.getText().toString().trim();
            String password = mActivityRegisterBinding.passwordEditText.getText().toString().trim();

            if (!email.isEmpty() && !password.isEmpty() && !userName.isEmpty()) {
                mPresenter.signUpWithEmailAndPassword(userName, email, password);
            } else {
                Toast.makeText(this, getString(R.string.activity_check_fields), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void setPresenter(Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showProgressBar() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.dialog_message));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    public void dismissProgressBar() {
        mProgressDialog.dismiss();
    }

    @Override
    public void onRegisterSuccess() {
        finish();
        Toast.makeText(this, getString(R.string.activity_register_user_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRegisterFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
