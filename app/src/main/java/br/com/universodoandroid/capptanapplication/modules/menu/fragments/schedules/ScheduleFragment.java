package br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.universodoandroid.capptanapplication.R;
import br.com.universodoandroid.capptanapplication.databinding.FragmentScheduleOpenBinding;
import br.com.universodoandroid.capptanapplication.modules.menu.MenuActivity;
import br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules.adapter.ScheduleAdapter;
import br.com.universodoandroid.capptanapplication.modules.registerschedule.NewScheduleActivity;
import br.com.universodoandroid.capptanapplication.services.FirebaseDatabaseProvider;

import static br.com.universodoandroid.capptanapplication.constants.Constants.STATUS_TRUE;
import static com.annimon.stream.Optional.ofNullable;

public abstract class ScheduleFragment extends Fragment implements ScheduleContract.View {

    private FragmentScheduleOpenBinding mFragmentDashboardBinding;
    private ScheduleContract.Presenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new SchedulePresenter(this, new FirebaseDatabaseProvider(getActivity()));

        ofNullable(((MenuActivity) getActivity())).ifPresent(activity -> activity.updateToolbarTitle("Dashboard"));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentDashboardBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_schedule_open, container, false);
        mFragmentDashboardBinding.setHandler(this);

        if (getStatusView().equals(STATUS_TRUE)) {
            mFragmentDashboardBinding.addNewScheduleButton.setVisibility(View.GONE);
        }

        mFragmentDashboardBinding.addNewScheduleButton.setOnClickListener(v ->
                getActivity().startActivity(new Intent(getActivity(), NewScheduleActivity.class)));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerView = mFragmentDashboardBinding.schedulersRecyclerView;
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(getSchedulersAdapter());

        mPresenter.requestSchedules();

        return mFragmentDashboardBinding.getRoot();
    }

    @Override
    public void setPresenter(ScheduleContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showProgressBar() {
        mFragmentDashboardBinding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgressBar() {
        mFragmentDashboardBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setMessageEmptyVisibility(int visibility) {
        mFragmentDashboardBinding.emptyMessageTextView.setVisibility(visibility);
    }

    protected abstract ScheduleAdapter getSchedulersAdapter();

    protected abstract String getStatusView();

    public ScheduleContract.Presenter getPresenter(){
        return mPresenter;
    }

}
