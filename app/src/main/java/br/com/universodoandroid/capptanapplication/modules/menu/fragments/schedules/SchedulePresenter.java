package br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules;

import android.support.annotation.NonNull;

import java.util.List;

import br.com.universodoandroid.capptanapplication.domain.Schedule;
import br.com.universodoandroid.capptanapplication.services.FirebaseDatabaseProvider;
import br.com.universodoandroid.capptanapplication.utils.RequestCallback;

public class SchedulePresenter implements ScheduleContract.Presenter {

    private ScheduleContract.View mView;
    private FirebaseDatabaseProvider mFirebaseDatabaseProvider;

    public SchedulePresenter(@NonNull ScheduleContract.View view,
                             @NonNull FirebaseDatabaseProvider firebaseDatabaseProvider) {
        mView = view;
        mFirebaseDatabaseProvider = firebaseDatabaseProvider;

        mView.setPresenter(this);
    }

    @Override
    public void requestSchedules() {
        mView.showProgressBar();

        mFirebaseDatabaseProvider.provideSchedules(new RequestCallback<List<Schedule>>() {
            @Override
            public void onSuccess(List<Schedule> schedules) {
                mView.loadSchedulers(schedules);
                mView.dismissProgressBar();
            }

            @Override
            public void onError(String errorMessage) {
                mView.dismissProgressBar();
            }
        });
    }

    @Override
    public void updateScheduleStatus(String scheduleId, String status) {
        mFirebaseDatabaseProvider.updateScheduleStatus(scheduleId, status);
    }

}
