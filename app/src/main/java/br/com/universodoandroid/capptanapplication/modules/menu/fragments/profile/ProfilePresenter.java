package br.com.universodoandroid.capptanapplication.modules.menu.fragments.profile;

import android.support.annotation.NonNull;
import android.util.Log;

import br.com.universodoandroid.capptanapplication.database.Session;
import br.com.universodoandroid.capptanapplication.domain.User;
import br.com.universodoandroid.capptanapplication.services.FirebaseAuthProvider;
import br.com.universodoandroid.capptanapplication.services.FirebaseDatabaseProvider;
import br.com.universodoandroid.capptanapplication.utils.RequestCallback;

class ProfilePresenter implements ProfileContract.Presenter {

    private FirebaseAuthProvider mFirebaseAuthProvider;
    private Session mSession;
    private FirebaseDatabaseProvider mFirebaseDatabaseProvider;
    private ProfileContract.View mView;

    public ProfilePresenter(@NonNull ProfileContract.View view,
                            @NonNull FirebaseAuthProvider firebaseAuthProvider,
                            @NonNull FirebaseDatabaseProvider firebaseDatabaseProvider,
                            @NonNull Session session) {
        mView = view;
        mFirebaseAuthProvider = firebaseAuthProvider;
        mFirebaseDatabaseProvider = firebaseDatabaseProvider;
        mSession = session;

        mView.setPresenter(this);
    }

    @Override
    public void getUserInformation() {
        mView.showProgressBar();

        mFirebaseDatabaseProvider.getUserInformation(new RequestCallback<User>() {
            @Override
            public void onSuccess(User user) {
                mView.setUserInformation(user);
                mView.dismissProgressBar();
            }

            @Override
            public void onError(String errorMessage) {
                Log.e(this.getClass().toString(), errorMessage);
            }
        });
    }

    @Override
    public void logout() {
        mFirebaseAuthProvider.logout(mSession);
    }

}
