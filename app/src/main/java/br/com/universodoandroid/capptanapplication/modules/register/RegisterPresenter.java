package br.com.universodoandroid.capptanapplication.modules.register;

import android.support.annotation.NonNull;

import br.com.universodoandroid.capptanapplication.services.FirebaseAuthProvider;
import br.com.universodoandroid.capptanapplication.services.FirebaseDatabaseProvider;
import br.com.universodoandroid.capptanapplication.utils.RequestCallback;

public class RegisterPresenter implements RegisterContract.Presenter {

    private FirebaseDatabaseProvider mFirebaseDatabaseProvider;
    private FirebaseAuthProvider mFirebaseAuthProvider;
    private RegisterContract.View mView;

    public RegisterPresenter(@NonNull RegisterContract.View view,
                             @NonNull FirebaseAuthProvider firebaseAuthProvider,
                             @NonNull FirebaseDatabaseProvider firebaseDatabaseProvider) {
        mView = view;
        mFirebaseAuthProvider = firebaseAuthProvider;
        mFirebaseDatabaseProvider = firebaseDatabaseProvider;

        mView.setPresenter(this);
    }

    @Override
    public void signUpWithEmailAndPassword(String name, String email, String password) {
        mView.showProgressBar();
        mFirebaseAuthProvider.signUpWithEmailAndPassword(email, password, new RequestCallback<String>() {
            @Override
            public void onSuccess(String userUid) {
                registerUserNameOnDatabase(userUid, name);

                mView.onRegisterSuccess();
                mView.dismissProgressBar();
            }

            @Override
            public void onError(String errorMessage) {
                mView.onRegisterFailure(errorMessage);
                mView.dismissProgressBar();
            }
        });
    }

    private void registerUserNameOnDatabase(String uid, String name) {
        mFirebaseDatabaseProvider.registerUserInformation(uid, name);
    }

}
