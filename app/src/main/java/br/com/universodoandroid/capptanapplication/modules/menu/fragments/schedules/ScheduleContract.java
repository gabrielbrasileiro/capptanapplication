package br.com.universodoandroid.capptanapplication.modules.menu.fragments.schedules;

import java.util.List;

import br.com.universodoandroid.capptanapplication.domain.Schedule;
import br.com.universodoandroid.capptanapplication.modules.BaseView;

public interface ScheduleContract {
    interface View extends BaseView<Presenter> {
        void loadSchedulers(List<Schedule> schedules);
        void setMessageEmptyVisibility(int visibility);
    }

    interface Presenter {
        void requestSchedules();
        void updateScheduleStatus(String scheduleId, String status);
    }
}
