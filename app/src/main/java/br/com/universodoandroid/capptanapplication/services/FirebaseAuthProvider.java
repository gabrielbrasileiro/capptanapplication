package br.com.universodoandroid.capptanapplication.services;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

import br.com.universodoandroid.capptanapplication.database.Session;
import br.com.universodoandroid.capptanapplication.modules.login.LoginActivity;
import br.com.universodoandroid.capptanapplication.utils.RequestCallback;

public class FirebaseAuthProvider {

    private FirebaseAuth mAuth;
    private Activity mActivity;

    public FirebaseAuthProvider(Activity activity) {
        mActivity = activity;
        mAuth = FirebaseAuth.getInstance();
    }

    public void signInWithEmailAndPassword(@NonNull String email, @NonNull String password, RequestCallback<FirebaseUser> callback) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(mActivity, task -> {
            if (task.isSuccessful()) {
                callback.onSuccess(mAuth.getCurrentUser());
            } else {
                callback.onError(Objects.requireNonNull(task.getException()).getMessage());
            }
        });
    }

    public void signUpWithEmailAndPassword(@NonNull String email, @NonNull String password, RequestCallback<String> callback) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(mActivity, task -> {
            if (task.isSuccessful()) {
                callback.onSuccess(task.getResult().getUser().getUid());
            } else {
                callback.onError(Objects.requireNonNull(task.getException()).getMessage());
            }
        });
    }

    public void resetPassword(String email, RequestCallback<Void> callback) {
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                callback.onSuccess(null);
            } else {
                callback.onError(task.getException().getMessage());
            }
        });
    }

    public void logout(Session session) {
        mAuth.signOut();
        session.deleteStateLogin();
        mActivity.startActivity(new Intent(mActivity, LoginActivity.class));
        mActivity.finish();
    }

}
